# OpenMP -for- Labs

Let's learn parallel programming using OpenMP!

## Zip

ZIP is an archive file format that supports lossless data compression.

See [Wikipedia](https://en.wikipedia.org/wiki/Zip_(file_format)).

#### TODO:

1. Fork the repository to your own account.
2. Clone your forked repository on your computer.
3. Study the code using your favorite editor. Build the application on your computer and run it.
4. Duplicate the code and parallelize it using OpenMP tasks.
5. Compare performance between serial and parallel versions. Determine the speedup.


Anything missing? Ideas for improvements? Make a pull request.
